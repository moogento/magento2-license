<?php
namespace Moogento\License\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;

class Keys extends Template implements RendererInterface
{
    protected $_template = 'Moogento_License::system/config/keys.phtml';

    protected $_helper;
    protected $_helperUrl;

    /** @var  \Magento\Framework\Json\Helper\Data */
    protected $_jsonHelper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Moogento\License\Helper\Data $helper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Moogento\License\Helper\Url $helperUrl,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        array $data = []
    ) {
        $this->_helper     = $helper;
        $this->_jsonHelper = $jsonHelper;
        $this->_helperUrl  = $helperUrl;
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element)
    {
        $this->setElement($element);
        return $this->toHtml();
        $this->getUrl('moogento_license/license/process');
    }

    public function getKeysInfo()
    {
        $keys = $this->_helper->getKeysInfo(false);
        return $this->_jsonHelper->jsonEncode(['keys' => $keys]);
    }

    public function getServerHost()
    {
        $baseUrl = $this->_scopeConfig->getValue('web/unsecure/base_url');
        $this->_helperUrl->parseUrl($baseUrl);
        return $this->_helperUrl->getHost();
    }
}
