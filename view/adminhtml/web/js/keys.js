define([
    'knockout',
    'jquery',
    './moment',
    'mage/translate'
], function(ko, $, moment) {
    function LicenseKey(data) {
        var self = this;

        data = data || {};
        self.key = ko.observable(data.key);
        self.valid = ko.observable(data.valid);
        self.checking = ko.observable(false);
        self.expires = ko.observable(data.expires);
        self.module = ko.observable(data.module);
        self.planName = ko.observable(data.planName);
        self.productName = ko.observable(data.productName);
        self.hireExist = ko.observable(data.hireExist);
        self.ongoing = ko.observable(data.ongoing);
        self.plan_id = ko.observable(data.plan_id);
        self.upgradeLink = 'https://moogento.com/downloadable/customer/products';
        self.settings = function(settings) {
            if ((typeof settings !== 'undefined') && (typeof settings.modules !== 'undefined')) {
                return settings.modules;
            }

            return [];
        };

        self.settings(data.settings);

        self.update = function(data) {
            console.log(data);
            self.valid(data.valid);
            self.expires(data.expires);
            self.module(data.module);
            self.ongoing(data.ongoing);
            self.plan_id(data.plan_id);
            self.planName(data.planName);
            self.productName(data.productName);
            self.hireExist(data.hireExist);
        };

        self.hasModules = ko.computed(function() {
            return !!self.module();
        });

        self.getMessage = ko.computed(function() {
            if (self.hireExist()) {
            	if(self.productName()){
                	return 'Grab a higher ' + self.productName() + ' plan to activate more features';
                } else {
                	return 'Grab a higher plan to activate more features';
                }
            }
            return false;
        });

        self.hasExpired = ko.computed(function() {
            if (!self.expires()) {
                return true;
            }
            return moment(self.expires()).isBefore(moment());
        });
    }

    function KeyList(data) {
        var self = this;
        data = data || {};
        data.keys = data.keys || [];

        self.new_key = ko.observable();
        self.error = ko.observable();

        self.keys = ko.observableArray();
        ko.utils.arrayForEach(data.keys, function(keyData) {
            self.keys.push(new LicenseKey(keyData));
        });

        self.absorbEnter = function(data, event) {
            return event.keyCode !== 13;
        };

        self.addKey = function() {
            self.error('');
            if (self.new_key()) {
                var existing = ko.utils.arrayFirst(self.keys(), function(k) {
                    return k.key() == self.new_key();
                });
                if (!existing) {
                    var key = new LicenseKey({key: self.new_key()});
                    self._addKey(key);
                    self.keys.push(key);
                    self.new_key('');
                } else {
                    self.error($.mage.__('Key already added'));
                }
            } else {
                self.error($.mage.__('Enter your key'));
            }
        };

        self.removeKey = function(key) {
            self.keys.remove(key);
            $.ajax({
                url: window.key_url,
                data: {
                    license_key: key.key(),
                    action: 'delete'
                }
            });
        };

        self._addKey = function(key) {
            key.checking(true);
            $.ajax({
                url: window.key_url,
                data: {
                    license_key: key.key(),
                    action: 'add'
                },
                success: function(result) {
                    key.update(result.success ? result.data : []);
                    key.checking(false);
                }
            });
        };

        self.getTemplate = function() {
            return 'Moogento_License/keys';
        };
    }

    return function(config) {
        return new KeyList(config);
    };
});
