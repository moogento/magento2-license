<?php
namespace Moogento\License\Controller\Adminhtml;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Moogento\License\Helper\Data;
use Moogento\License\Exception\LicenseException;

abstract class AbstractAction extends \Magento\Backend\App\Action
{
    /**
     * @var string
     */
    private $nameModule = '';

    /**
     * @var string
     */
    private $messageNoLicense = '';
    /**
     * @var Data
     */
    private $helper;

    /**
     * AbstractAction constructor.
     * @param Context $context
     */
    public function __construct(Context $context) {
        parent::__construct($context);
        $this->helper = $context->getLicenseHelper();
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface|mixed
     */
    public function dispatch(RequestInterface $request)
    {
        $isPlanValid = $this->helper->isPlanValid($this->nameModule);
        if (!$isPlanValid) {
            $licenseMessage = $this->helper->getNoLicenseMessage(__($this->messageNoLicense));
            $this->messageManager->addErrorMessage($licenseMessage);
            return $this->getRedirectionBack();
        }

        try {
            return parent::dispatch($request);
        } catch (LicenseException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->getRedirectionBack();
        }
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    abstract public function execute();

    /**
     * @return mixed
     */
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function getRedirectionBack()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath($this->_redirect->getRefererUrl());
    }
}
