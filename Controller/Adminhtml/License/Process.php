<?php
namespace Moogento\License\Controller\Adminhtml\License;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class Process extends \Magento\Backend\App\Action
{
    const ACTION_ADD = 'add';
    const ACTION_DELETE = 'delete';

    protected $_licenseHelper;
    protected $_resultJsonFactory;
    protected $_jsonHelper;
    protected $_configModel;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Moogento\License\Helper\Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        JsonFactory $resultJsonFactory,
        \Magento\Config\Model\ConfigFactory $configFactory
    ) {
        $this->_licenseHelper     = $helper;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_jsonHelper        = $jsonHelper;
        $this->_configFactory     = $configFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $key    = $this->getRequest()->getParam('license_key');
        $action = $this->getRequest()->getParam('action');

        $keyData = [];
        switch ($action) {
            case self::ACTION_ADD:
                $keyData = $this->_licenseHelper->getFullKeyData([$key]);
                $this->_addKey($key);
                break;
            case self::ACTION_DELETE:
                $this->_licenseHelper->cleanKeyCache($key);
                $this->_removeKey($key);
                break;
        }

        $result = $this->_resultJsonFactory->create();
        return $result->setData(
            ['success' => true, 'data' => $keyData]
        );
    }

    protected function _saveKeys($keys)
    {
        $configData = [
            'section' => 'moogento_license',
            'website' => null,
            'store'   => null,
            'groups'  => [
                'keys' => [
                    'fields' => [
                        'list' => [
                            'value' => $this->_jsonHelper->jsonEncode($keys),
                        ],
                    ],
                ],
            ],
        ];

        $configModel = $this->_configFactory->create(['data' => $configData]);
        $configModel->save();
    }

    protected function _addKey($key)
    {
        $keys = $this->_licenseHelper->getScopeConfig()->getValue(
            'moogento_license/keys/list'
        );
        if ($keys) {
            $keys = $this->_jsonHelper->jsonDecode($keys);
        } else {
            $keys = [];
        }
        $keys[] = $key;
        $keys   = array_unique($keys);
        $this->_saveKeys($keys);
    }

    protected function _removeKey($key)
    {
        $keys = $this->_licenseHelper->getScopeConfig()->getValue(
            'moogento_license/keys/list'
        );
        if ($keys) {
            $keys = $this->_jsonHelper->jsonDecode($keys);
        } else {
            $keys = [];
        }
        $keys = array_diff($keys, [$key]);
        $this->_saveKeys($keys);
    }

}
